#!/usr/bin/env bash
chown -R mysql:mysql /var/lib/mysql
chmod -R 755 /var/lib/mysql
service php$1-fpm start
service nginx start
service cron start
service postgresql start
service mysql start
bash